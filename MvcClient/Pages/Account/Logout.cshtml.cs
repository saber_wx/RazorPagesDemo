using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MvcClient.Pages.Account
{
    public class LogoutModel : PageModel
    {
        private readonly ILogger<LogoutModel> _logger;
        /// <summary>
        /// The schemes
        /// </summary>
        protected readonly IAuthenticationSchemeProvider Schemes;

        /// <summary>
        /// The handlers
        /// </summary>
        protected readonly IAuthenticationHandlerProvider Handlers;
        private readonly IOptions<AuthenticationOptions> _authOptions;

        public LogoutModel(
            IAuthenticationSchemeProvider schemes,
            IAuthenticationHandlerProvider handlers,
            IOptions<AuthenticationOptions> options,
            ILogger<LogoutModel> logger)
        {
            
            _logger = logger;

            _authOptions = options;
            Schemes = schemes;
            Handlers = handlers;
        }


        public async Task<IActionResult> OnGetAsync()
        {
            // ===================================
            var opt = _authOptions;
            var defaultScheme = await Schemes.GetDefaultAuthenticateSchemeAsync();
            var s2 = await Schemes.GetDefaultChallengeSchemeAsync();
            var s3 = await Schemes.GetDefaultSignInSchemeAsync();
            var handler = await Handlers.GetHandlerAsync(HttpContext, defaultScheme.Name);
            var result2 = await handler.AuthenticateAsync();
            // ===================================

            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");

            return RedirectToPage("/Index");
        }
    }
}